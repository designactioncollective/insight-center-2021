<?php

namespace App;

/**
 * Theme customizer
 */
add_action('customize_register', function (\WP_Customize_Manager $wp_customize) {
    // Add postMessage support
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial('blogname', [
        'selector' => '.brand',
        'render_callback' => function () {
            bloginfo('name');
        }
    ]);
});

/**
 * Customizer JS
 */
add_action('customize_preview_init', function () {
    wp_enqueue_script('sage/customizer.js', asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
});

/**
* Add Social Media Links
*/
add_action('customize_register', function (\WP_Customize_Manager $wp_customize) {
    $wp_customize->add_section('social_media_links', array(
        'title' =>       'Social Media and External Links',
        'description' => '',
        'priority' =>    120,
    ));

    $wp_customize->add_setting( 'facebook_link' );
    $wp_customize->add_setting( 'twitter_link' );
    $wp_customize->add_setting( 'instagram_link' );
    $wp_customize->add_setting( 'youtube_link' );
    $wp_customize->add_setting( 'medium_link' );
    $wp_customize->add_setting( 'donation_link' );
    $wp_customize->add_setting( 'donation_title' );

    $wp_customize->add_control( 'facebook_link', array(
        'type' => 'url',
        'section' => 'social_media_links',
        'settings' => 'facebook_link',
        'label' => __( 'Facebook Link' ),
        'description' => __( 'Include a link to your facebook page.' ),
        )
    );

    $wp_customize->add_control( 'twitter_link', array(
        'type' => 'url',
        'section' => 'social_media_links',
        'settings' => 'twitter_link',
        'label' => __( 'Twitter Link' ),
        'description' => __( 'Include a link to your twitter page.' ),
        )
    );

    $wp_customize->add_control( 'instagram_link', array(
        'type' => 'url',
        'section' => 'social_media_links',
        'settings' => 'instagram_link',
        'label' => __( 'Instagram Link' ),
        'description' => __( 'Include a link to your instagram page.' ),
        )
    );

    $wp_customize->add_control( 'youtube_link', array(
        'type' => 'url',
        'section' => 'social_media_links',
        'settings' => 'youtube_link',
        'label' => __( 'Youtube Link' ),
        'description' => __( 'Include a link to your youtube page.' ),
        )
    );

    $wp_customize->add_control( 'medium_link', array(
        'type' => 'url',
        'section' => 'social_media_links',
        'settings' => 'medium_link',
        'label' => __( 'Medium Link' ),
        'description' => __( 'Include a link to your medium page.' ),
        )
    );

    $wp_customize->add_control( 'donation_link', array(
        'type' => 'url',
        'section' => 'social_media_links',
        'settings' => 'donation_link',
        'label' => __( 'Link to donation page' ),
        'description' => __( 'Include a link to your donation page.' ),
        )
    );

    $wp_customize->add_control( 'donation_title', array(
        'type' => 'url',
        'section' => 'social_media_links',
        'settings' => 'donation_title',
        'label' => __( 'Title of donation page link' ),
        'description' => __( 'Link title for the donation page.' ),
        )
    );
});
