export default {
  init() {
    // JavaScript to be fired on all pages
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
    // Owl Carousel
    jQuery('.owl-carousel').owlCarousel({
      loop: true,
      nav: true,
      navText: ['<i class="fas fa-arrow-circle-left"></i>', '<i class="fas fa-arrow-circle-right"></i>'],
      items: 1,
    });

    // Owl Carousel for Publications
    jQuery('.owl-carousel-publications').owlCarousel({
      loop: true,
      nav: true,
      navText: ['<i class="fas fa-arrow-circle-left"></i>', '<i class="fas fa-arrow-circle-right"></i>'],
      items: 1,
    });


    //Hamburger Menu
    var $hamburger = jQuery('.hamburger');
    $hamburger.on('click', function() {
      $hamburger.toggleClass('is-active');
      // Do something else, like open/close menu
    });
    jQuery('#headerbar__menu').on('hidden.bs.modal', function () {
      $hamburger.toggleClass('is-active');
    })
  },
};
