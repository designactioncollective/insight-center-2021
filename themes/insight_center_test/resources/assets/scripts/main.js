// import external dependencies
import 'jquery';

// Import everything from autoload
import './autoload/**/*'


// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';

import 'owl.carousel';

// import then needed Font Awesome functionality
import { library, dom } from '@fortawesome/fontawesome-svg-core';

//Regular icons
import { faEnvelopeOpen } from '@fortawesome/free-regular-svg-icons';

// import the Facebook and Twitter icons
import { faFacebook, faTwitter, faInstagram, faFacebookF, faYoutube, faMediumM } from '@fortawesome/free-brands-svg-icons';

// import the Solid icons
import { faArrowCircleLeft, faArrowCircleRight, faEnvelope, faTimes } from '@fortawesome/pro-solid-svg-icons';

//Import light pro
import { faHandHoldingUsd, faSearch } from '@fortawesome/pro-light-svg-icons';

// add the imported icons to the library
library.add(faFacebook, faTwitter, faInstagram, faArrowCircleRight, faArrowCircleLeft, faEnvelope,  faFacebookF, faSearch, faTimes , faEnvelopeOpen, faHandHoldingUsd, faYoutube, faMediumM );

// tell FontAwesome to watch the DOM and add the SVGs when it detects icon markup
dom.watch();


/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
