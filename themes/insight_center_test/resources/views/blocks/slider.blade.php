{{--
  Title: Slider
  Description: Slider with captions and descriptions
  Category: layout
  Icon: slides
  Keywords: slider slideshow
  supportsAlign: wide full
--}}
<div data-{{ $block['id'] }} class="{{ $block['classes'] }}">
  @if (have_rows('slides'))
    <div class="owl-carousel owl-carousel--main">
      @while (have_rows('slides'))
        @php
          the_row();
          $background = get_sub_field('background');
          $title      = get_sub_field('title');
          $caption    = get_sub_field('caption');
          $buttons    = get_sub_field('buttons');
        @endphp
        <div class="slide">
          <figure class="slide__background">
            <div class="slide__background__image" <?php if($background) { echo 'style="background-image:url(' . $background['sizes']['slider'] . ')"'; } ?>></div>
            <figcaption class="slide__caption-wrap">
              <h2 class="slide__title">{!! $title !!}</h2>

              @if ($caption)
                <p class="slide__caption">{!! $caption !!}</p>
              @endif

              @if ($buttons)
                <ul class="slide__btns">
                  @foreach ($buttons as $button)
                    <li><a href="{!! $button['button']['url'] !!}" target="{!! $button['button']['target'] !!}">{!! $button['button']['title'] !!} <i class="fal fa-arrow-right"></i></a></li>
                  @endforeach
                </ul>
              @endif
            </figcaption>
          </figure>
        </div>
      @endwhile
    </div>
  @endif
</div>
