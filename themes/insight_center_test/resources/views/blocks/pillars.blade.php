{{--
  Title: Pillars Set
  Description: Group of three cards that comes from pillars
  Category: layout
  Icon: grid-view
  Keywords: pillar
--}}
<div  data-{{ $block['id'] }} class="{{ $block['classes'] }}" style="background-image: url(@asset('images/ourwork-bg.png')); background-repeat: no-repeat;">
  <div class="container container__ourwork">
      <h3>{{ get_field('title') }}</h3>
      <div>
        <p>{{ get_field('introduction') }}</p>
      </div>
      <ul class="pillars-cards  d-flex list-unstyled justify-content-between align-items-stretch">
        <?php
          // Get Pillars
          $terms = get_terms( array(
              'taxonomy' => 'pillar',
              'hide_empty' => false,
          ) );

          foreach ( $terms as $term ) {
            echo '<li>';
            $term_link = get_term_link( $term );
            echo '<a href="'.$term_link.'"  class="d-block">';
              $image = get_field('image', $term);
                  if( $image ): ?>
                  	<div class="card-header"
                  		style= 'position:relative; background-image: url("<?php echo $image['url']; ?>")'>
                    <div class="overlay overlay--brown"></div>
                  	</div>

              	<?php endif; ?>
              <?php
              echo '<p>' . $term->name . '</p>';
            echo '</a></li>';
          }
        ?>
      </ul>
  </div>
</div>
