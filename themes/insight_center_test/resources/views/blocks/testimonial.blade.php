{{--
  Title: Testimonial
  Description: Customer testimonial
  Category: layout
  Icon: admin-comments
  Keywords: testimonial quote
--}}

<blockquote data-{{ $block['id'] }} class="{{ $block['classes'] }}">
    <p>{{ get_field('testimonial') }}</p>
    <cite>
      <span>{{ get_field('author') }}</span>
    </cite>
</blockquote>
