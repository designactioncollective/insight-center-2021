{{--
  Title: Publications Slider
  Description: Slide with latest publications. They require a featured image and a quote
  Category: layout
  Icon: slides
  Keywords: slider publication
--}}

<blockquote data-{{ $block['id'] }} class="{{ $block['classes'] }}" style="background: url(@asset('images/publications-bg.png')) no-repeat center top;" >
   <div class="container container__publications">
      <?php
      $publications = get_field('publications');
      if( $publications ): ?>
        <div class="owl-carousel-publications owl-carousel owl-theme">
          <?php foreach( $publications as $publication ):
              $permalink = get_permalink( $publication->ID );
              $title = get_the_title( $publication->ID );
              $custom_field = get_field( 'quote', $publication->ID );
              $featured_img_url = get_the_post_thumbnail_url($publication->ID, 'large');
              ?>
              <div class="publication-slide">
                  <div>
                    <h4><?php the_field('section_title'); ?></h4>
                    <p><quote><?php the_field('quote', $publication->ID ); ?></quote></p>
                  </div>
                  <div class="publication-cover" style="background-image:url(<?php echo $featured_img_url; ?>) ">
                    <?php //echo get_the_post_thumbnail( $publication->ID, 'large', array( 'class' => 'alignright' ) ); ?>
                    <a href="<?php echo esc_url( $permalink ); ?>"><?php echo esc_html( $title ); ?></a>
                  </div>
              </div>
          <?php endforeach; ?>
        </div>
      <?php endif; ?>
   </div>
</blockquote>
