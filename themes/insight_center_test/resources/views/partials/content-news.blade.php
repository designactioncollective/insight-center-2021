<article @php post_class() @endphp>
  <a href="{{ get_permalink() }}">
    <header>
      <?php the_post_thumbnail("medium"); ?>
      <h4 class="entry-title">{!! get_the_title() !!}</h4>
    </header>
  </a>
    <div class="article__meta">
      <?php
        // By name | News Source
        if( get_field('by') ) : the_field('by');  endif;
        if( get_field('source') ) : echo " | ";  the_field('source'); endif;
      ?>
    </div>
    <div class="entry-summary">
      @php the_excerpt() @endphp
    </div>
</article>
