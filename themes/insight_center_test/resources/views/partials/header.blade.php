<header class="banner">
  <div class="container">
    @if ( function_exists( 'the_custom_logo' ) )
      {!! the_custom_logo() !!}
    @else :
      <a class="brand" href="{{ home_url('/') }}">{{ get_bloginfo('name', 'display') }}</a>
    @endif
  </div>
</header>
