<footer class="content-info">
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        @include('partials.social-media-icons')
      </div>
      <div class="col-sm-3">
        @php dynamic_sidebar('sidebar-footer') @endphp
      </div>
      <div class="col-sm-3">
        {!! the_custom_logo() !!}
      </div>
    </div>
  </div>
</footer>
