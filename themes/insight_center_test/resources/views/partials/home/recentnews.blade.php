<?php

  $frontpage_id = get_option( 'page_on_front' );

  //Get the last 3 News (Media Coregae and Op-eds)
  $args = array(
    'post_type'          => array('news', 'post'),
    'posts_per_page'     => 3,
    'tax_query' => array(
        array(
            'taxonomy' => 'resources_type',
            'field'    => 'slug',
            'terms'    => array('coverage','opeds'),
        ),
      ),
  );

  $news = new WP_Query($args);

  if( $news->have_posts() ): ?>
    <h3 class="section-title text-center">{{ __('Recent News', 'insightcenter') }}</h3>
    <p class="news__description text-center"><?php if( get_field('description', $frontpage_id) ) the_field('description', $frontpage_id ); ?></p>
    <div class="row">
      <?php
      while( $news->have_posts() ) :
          $news->the_post(); ?>
          <div class="col-sm-4">
            @include('partials.content-news')
          </div>
          <?php
      endwhile; ?>
    </div>
    <?php
  endif;

?>
