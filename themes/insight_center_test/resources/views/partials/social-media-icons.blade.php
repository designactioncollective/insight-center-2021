 <?php
   //get links from Customizer
        $donate_link = get_theme_mod("donation_link", "");
        $donate_title = get_theme_mod("donation_title", "");
        $fb_link = get_theme_mod("facebook_link", "");
        $tw_link = get_theme_mod("twitter_link", "");
        $ig_link = get_theme_mod("instagram_link", "");
        $youtube = get_theme_mod("youtube_link", "");
 ?>

 <ul class="social-media-icons">
  <!-- Facebook icon -->
  <li><a target ="_blank" href="<?php echo $fb_link; ?>" class="header__social-icons--link fb"><i class="fab fa-facebook-f"></i></a></li>
  <!-- Twitter icon -->
  <li><a target="_blank" href="<?php echo $twitter; ?>" class="header__social-icons--link tw"><i class="fab fa-twitter"></i></a></li>
  <!-- Medium icon -->
  <li><a href="#" class="header__social-icons--link medium"><i class="fab fa-medium-m"></i></a></li>
    <!-- Youtube icon -->
  <li><a href="#" class="header__social-icons---link youtube"><i class="fab fa-youtube"></i></a></li>

