<div id="headerbar">
  <button  tabindex="0" aria-label="Menu" data-toggle="modal" data-target="#headerbar__menu"  aria-controls="navigation" >
    <div class="hamburger hamburger--minus"  role="button">
        <div class="hamburger-box">
          <div class="hamburger-inner"></div>
        </div>
    </div>
  </button>
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#headerbar__search" ><i class="fal fa-search"></i></button>
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#headerbar__email" ><i class="far fa-envelope-open"></i></button>
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#headerbar_donate" ><i class="fal fa-hand-holding-usd"></i></button>


  <div class="modal fade" id="headerbar__menu" tabindex="-1" role="dialog" aria-labelledby="headerbar__menuLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="modal-title" id="headerbar__menuLabel">
            @if ( function_exists( 'the_custom_logo' ) )
              {!! the_custom_logo() !!}
            @else :
              <a class="brand" href="{{ home_url('/') }}">{{ get_bloginfo('name', 'display') }}</a>
            @endif
          </div>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <nav class="nav-primary">
            @if (has_nav_menu('primary_navigation'))
              {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'slimmenu']) !!}
            @endif
          </nav>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="headerbar__search" tabindex="-1" role="dialog" aria-labelledby="headerbar__searchLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="modal-title" id="headerbar__menuLabel">
            @if ( function_exists( 'the_custom_logo' ) )
              {!! the_custom_logo() !!}
            @else :
              <a class="brand" href="{{ home_url('/') }}">{{ get_bloginfo('name', 'display') }}</a>
            @endif
          </div>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
            <div id="search"><?php get_search_form(); ?><i class="fal fa-search"></i></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="headerbar__email" tabindex="-1" role="dialog" aria-labelledby="headerbar__emailLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="headerbar__emailLabel">Email</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="headerbar_donate" tabindex="-1" role="dialog" aria-labelledby="headerbar_donateLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="headerbar_donateLabel">Donate</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

