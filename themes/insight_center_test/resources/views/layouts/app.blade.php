<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <?php //get background image for homepage
    if( is_front_page() ) :
      $id = get_option( 'page_on_front' );

      if( get_field('background_image', $id) ) {
        $img_bg_url = get_field('background_image',$id);
      }
    endif;
  ?>
  <body @php body_class() @endphp <?php if( isset($img_bg_url) ) echo ' style="background: url(' . $img_bg_url .') no-repeat"'; ?>>
    @php do_action('get_header') @endphp
    @include('partials.header')
    <div class="wrap container" role="document">
      <div class="content">
        <main class="main">
          @yield('content')
        </main>
        @include('partials.header-bar')
        @if (App\display_sidebar())
          <aside class="sidebar">
            @include('partials.sidebar')
          </aside>
        @endif
      </div>
    </div>
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
  </body>
</html>
