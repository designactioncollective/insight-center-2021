<?php
/**
 * Plugin Name: Design Action plugins
 * Description: A collection of must-use plugins
 * Author: Design Action Collective
*/


/*
 * Load all mu-plugins
*/

/* Functions */
require_once(dirname(__FILE__) . '/dac/functions/sidebar-menus.php');
//require_once(dirname(__FILE__) . '/dac/functions/menu-shortcode.php');
//require_once(dirname(__FILE__) . '/dac/functions/acf_share_options.php');
//require_once(dirname(__FILE__) . '/dac/functions/pagination.php');
//require_once(dirname(__FILE__) . '/dac/functions/site_links.php');
require_once(dirname(__FILE__) . '/dac/functions/theme_colors.php');
require_once(dirname(__FILE__) . '/dac/functions/breadcrumbs.php');


/* Custom post types and taxonomies */
require_once(dirname(__FILE__) . '/dac/custom-types-tax/dac-staff-board.php');
require_once(dirname(__FILE__) . '/dac/custom-types-tax/news.php');
require_once(dirname(__FILE__) . '/dac/custom-types-tax/resources.php');
require_once(dirname(__FILE__) . '/dac/custom-types-tax/resources-types.php');
require_once(dirname(__FILE__) . '/dac/custom-types-tax/events.php');
require_once(dirname(__FILE__) . '/dac/custom-types-tax/voices.php');
require_once(dirname(__FILE__) . '/dac/custom-types-tax/oldpages.php');
require_once(dirname(__FILE__) . '/dac/custom-types-tax/tax-pillars.php');

