<?php

function the_breadcrumb() {
    global $post;
    echo '<ul id="breadcrumbs">';
    if (!is_home()) {
        echo '<li><a href="';
        echo get_option('home');
        echo '">';
        echo 'Home';
        echo '</a></li><li class="separator"> / </li>';
        if (is_category() || is_single() || is_tax()) {
            echo '<li>';
            if( !is_singular( array('page', 'attachment', 'post') ) && !is_tax() ){
            	$post_type = get_post_type_object( get_post_type($post) );
            	if (function_exists('tribe_is_event')){
	            	if(tribe_is_event()) {
            			echo '<a href="' . tribe_get_events_link() . '">' . $post_type->labels->menu_name . '</a></li><li> ';
            		} 
            	}
            	$post_type_string = get_post_type($post->ID);
            	if(  $post_type_string == 'resource' ) {
	            		$parent_url = get_permalink( get_page_by_path( 'resources' ) );
	            		echo '<a href="'.$parent_url.'">'; _e('Resources', 'todec');  echo '</a></li><li> ';
	            }
	            else if(  $post_type_string == 'program' ){
		            		if( $post->post_parent ){

			            		$grantparent_url = get_permalink( get_page_by_path( 'programs/program-history' ) );
			            		$parent_url = get_permalink( wp_get_post_parent_id($post->ID) );
			            		echo '<a href="'.$grantparent_url.'">' . 'Program History' . '</a></li><li class="separator"> / </li><li> ';
								echo '<a href="'.$parent_url.'">' . get_the_title(wp_get_post_parent_id($post->ID)) . '</a></li><li> ';

		            		}
		            		else{
			            		$grantparent_url = get_permalink( get_page_by_path( 'programs/program-history' ) );
			            		$parent_url = get_permalink( get_page_by_path( 'programs' ) );
		            			echo '<a href="'.$parent_url.'">' . 'Programs' . '</a></li><li><li class="separator"> / </li><li> ';
		            			echo '<a href="'.$grantparent_url.'">' . 'Program History' . '</a></li><li> ';
		            		}

	            }
	            else if ( $post_type_string == 'tools' ) {
						$parent_url = get_permalink( get_page_by_path( 'our-work/training-leadership-development/organizingtools' ) );
						$grantparent_url = get_permalink( get_page_by_path( 'resources' ) );
						echo '<a href="'.$grantparent_url.'">'; _e('Resources','todec'); echo '</a></li><li><li class="separator"> / </li><li> ';
	            		echo '<a href="'.$parent_url.'">' ; _e('Organizing Tools','todec'); echo '</a></li><li> ';		            
	            }
	            else if ( $post_type_string == 'discussion-topics' ) {
	            		$parent_url = get_permalink( get_page_by_path( 'members-portal' ) );
	            		echo '<a href="'.$parent_url.'">'; _e('Discussion Board', 'todec');  echo '</a></li><li> ';		            		            
	            }
	            else if ( $post_type_string == 'member_project' ) {
	            		$parent_url = get_permalink( get_page_by_path( 'our-work/member-initiated-projects' ) );
	            		echo '<a href="'.$parent_url.'">'; _e('Member Initiated Projects', 'todec');  echo '</a></li><li> ';		            		            
	            }
	            else if ( $post_type_string == 'people' ) {
	            		$parent_url = get_permalink( get_page_by_path( 'our-story' ) );
	            		echo '<a href="'.$parent_url.'">'; _e('Our Story', 'todec');  echo '</a></li><li> ';	
	            		// Get Category for People Staff or Board 
	            		$terms = get_the_terms( $post->ID , 'people-category' );	
	            		$cat_url = get_permalink( get_page_by_path( 'our-story/our-team' ) );
	            		$name = 'Our Team';
	            		if ( $terms && ! is_wp_error( $terms ) ) : 
		            		foreach ($terms as $term ):
		            			
		            			if( $term->slug == "staff-cat" ){
			            			$cat_url = get_permalink( get_page_by_path( 'who-we-are/staff' ) );
			            			$name = 'Staff';
		            			}
		            			else if( $term->slug == "board-cat" ){
			            			$cat_url = get_permalink( get_page_by_path( 'who-we-are/board' ) );
			            			$name = 'Board';
		            			}
		            		endforeach;
	            		endif;  
	            		echo '<li class="separator"> / </li><li><a href="'.$cat_url.'">' ; _e($name,'todec'); echo '</a></li><li> ';	          		            
	            }	 
				else if ( $post_type_string == 'chapter' ) {
	            		$parent_url = get_permalink( get_page_by_path( 'our-work/freefromfear/our-chapters' ) );
	            		echo '<a href="'.$parent_url.'">'; _e('Our Chapters', 'todec');  echo '</a></li><li> ';		            		            
	            }    
				else if ( $post_type_string == 'research_post' ) {
	            		$parent_url = get_permalink( get_page_by_path( 'research/' ) );
	            		echo '<a href="'.$parent_url.'">'; _e('Research', 'todec');  echo '</a></li><li> ';		            		            
	            }	                   	            
	            else {
						echo '<a href="' . get_post_type_archive_link( get_post_type($post) ) . '">' . $post_type->labels->menu_name . '</a></li><li> ';
            	}
            }
            elseif (is_tax()) {
            	$post_type = get_post_type_object( get_post_type($post) );
            	$slug = strtolower($post_type->labels->name);
            	//$url = get_bloginfo('url') . '/' . $slug;
            	$url = get_post_type_archive_link( $post_type->rewrite['slug'] );
				echo '<a href="' . $url . '">' . $post_type->labels->menu_name . '</a></li> <li class="separator">/</li> <li>';
            	global $wp_query;
  						  	$term = $wp_query->get_queried_object();
    						$title = $term->name;
    						$term_link = get_term_link( $term );
            	echo '<span title="'.$title.'">'.$title.'</span>';
            }
            else {
	            //check if it is category News and Updates and have the link go to the Updates page, same with Blog
	            $page_url = get_permalink( get_page_by_path( 'updates' ) );
	            $page_url_blog = get_permalink( get_page_by_path( 'blog-feed' ) );
	            if( in_category("news-and-updates") )
	            	echo '</li><li class="separator"> / </li><li><a href="'. $page_url . '">Updates</a>';
	            elseif( in_category("all-posts") ) {
		          	echo '</li><li class="separator"> / </li><li><a href="'. $page_url_blog . '">'; _e('Blog','todec'); echo '</a>'; }
				else
		            the_category(' </li><li class="separator"> / </li><li> ');

	            }
            if (is_single()) {
                echo '</li><li class="separator"> / </li><li>';
                if( is_singular('campaigns_programs') ){
	                // find the category
	                $terms = get_the_terms( get_the_ID(), 'campaigns_programs-category' );
                    if ( $terms && ! is_wp_error( $terms ) ) : 
                    	foreach ( $terms as $term ) {
	                    	echo '<a href="'.get_term_link($term->term_id).'">'.$term->name.'</a>';
                    	}
                    else :
                    	the_title();
						
                    endif;
                }
                else 
                	the_title();
                echo '</li>';
            }
        } elseif (is_page()) {
            if($post->post_parent){
                $anc = array_reverse( get_post_ancestors( $post->ID ) );
                $title = get_the_title();
                foreach ( $anc as $ancestor ) {

                    echo $output = '<li><a href="'.get_permalink($ancestor).'" title="'.get_the_title($ancestor).'">'.get_the_title($ancestor).'</a></li><li class="separator">/</li>';
                }
                echo '<span title="'.$title.'">'.$title.'</span>';
            } else {
                echo '<span>';
                echo the_title();
                echo '</span>';
            }
        }

    elseif (is_tag()) {single_tag_title();}
    elseif (is_day()) {echo"<li>Archive for "; the_time('F jS, Y'); echo'</li>';}
    elseif (is_month()) {echo"<li>Archive for "; the_time('F, Y'); echo'</li>';}
    elseif (is_year()) {echo"<li>Archive for "; the_time('Y'); echo'</li>';}
    elseif (is_author()) {echo"<li>Author Archive"; echo'</li>';}
    elseif (is_post_type_archive()) {
    	$post_type = get_post_type_object( get_post_type($post) );
		echo $post_type->labels->menu_name; }
    elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<li>Blog Archives"; echo'</li>';}
    elseif (is_search()) {echo"<li>Search Results"; echo'</li>';}
    elseif (function_exists('tribe_is_event')) { if(tribe_is_month() || tribe_is_past() || tribe_is_upcoming() || tribe_is_day()) {
    	echo "<li>Events"; echo "</li>";
    }}
    }
   echo '</ul>';
}
