<?php
	
// Adds support for editor color palette.
add_theme_support( 'editor-color-palette', array(
	array(
		'name'  => __( 'Yellow', 'todec' ),
		'slug'  => 'yellow',
		'color'	=> '#FCB040',
	),
	array(
		'name'  => __( 'Dark Blue', 'todec' ),
		'slug'  => 'dark-blue',
		'color' => '#1C396B',
	),
	array(
		'name'  => __( 'Light Blue', 'todec' ),
		'slug'  => 'light-blue',
		'color' => '#3A8AC5',
	),
	
	array(
		'name'  => __( 'Dark Grey', 'todec' ),
		'slug'  => 'dark-grey',
		'color' => '#212529',
	),
	array(
		'name'  => __( 'Black', 'todec' ),
		'slug'  => 'black',
		'color' => '#000',
	),	
	array(
		'name'  => __( 'White', 'todec' ),
		'slug'  => 'white',
		'color' => '#fff',
	),	
) );