<?php
/*
 * Modifying TinyMCE editor to remove unused items.
 */
function myformatTinyMCE($in) {
  $in['block_formats'] = "Paragraph=p;Header 3=h3;Header 4=h4;Header 5=h5;Header 6=h6";
  return $in;
}
add_filter('tiny_mce_before_init', 'myformatTinyMCE' );