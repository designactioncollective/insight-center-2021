<?php
/*
Plugin Name: DAC - News CPT
Description: <strong>News</strong> Functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// Register Custom Post Type
function custom_post_type() {

	$labels = array(
		'name'                  => _x( 'News', 'Post Type General Name', 'todec' ),
		'singular_name'         => _x( 'News', 'Post Type Singular Name', 'todec' ),
		'menu_name'             => __( 'News', 'todec' ),
		'name_admin_bar'        => __( 'News', 'todec' ),
		'archives'              => __( 'News Archives', 'todec' ),
		'attributes'            => __( 'News Attributes', 'todec' ),
		'parent_item_colon'     => __( 'Parent News:', 'todec' ),
		'all_items'             => __( 'All News', 'todec' ),
		'add_new_item'          => __( 'Add New Campaign or Program', 'todec' ),
		'add_new'               => __( 'Add New News', 'todec' ),
		'new_item'              => __( 'New News', 'todec' ),
		'edit_item'             => __( 'Edit News', 'todec' ),
		'update_item'           => __( 'Update News', 'todec' ),
		'view_item'             => __( 'View News', 'todec' ),
		'view_items'            => __( 'View News', 'todec' ),
		'search_items'          => __( 'Search News', 'todec' ),
		'not_found'             => __( 'Not found', 'todec' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'todec' ),
		'featured_image'        => __( 'Featured Image', 'todec' ),
		'set_featured_image'    => __( 'Set featured image', 'todec' ),
		'remove_featured_image' => __( 'Remove featured image', 'todec' ),
		'use_featured_image'    => __( 'Use as featured image', 'todec' ),
		'insert_into_item'      => __( 'Insert into News', 'todec' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'todec' ),
		'items_list'            => __( 'News list', 'todec' ),
		'items_list_navigation' => __( 'News list navigation', 'todec' ),
		'filter_items_list'     => __( 'Filter News list', 'todec' ),
	);
	$args = array(
		'label'                 => __( 'News', 'todec' ),
		'description'           => __( 'Post Type Description', 'todec' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', 'excerpt' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'news', $args );

}
add_action( 'init', 'custom_post_type', 0 );
