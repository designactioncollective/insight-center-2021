<?php
/*
Plugin Name: DAC - Types
Description: <strong>Resources Types</strong> Functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );


// Register Custom Taxonomy
function custom_taxonomy_type() {

	$labels = array(
		'name'                       => _x( 'Types', 'Taxonomy General Name', 'insight' ),
		'singular_name'              => _x( 'Type', 'Taxonomy Singular Name', 'insight' ),
		'menu_name'                  => __( 'Types', 'insight' ),
		'all_items'                  => __( 'All Types', 'insight' ),
		'parent_item'                => __( 'Parent Type', 'insight' ),
		'parent_item_colon'          => __( 'Parent Type:', 'insight' ),
		'new_item_name'              => __( 'New Type Name', 'insight' ),
		'add_new_item'               => __( 'Add New Type', 'insight' ),
		'edit_item'                  => __( 'Edit Type', 'insight' ),
		'update_item'                => __( 'Update Type', 'insight' ),
		'view_item'                  => __( 'View Type', 'insight' ),
		'separate_items_with_commas' => __( 'Separate Type with commas', 'insight' ),
		'add_or_remove_items'        => __( 'Add or remove Type', 'insight' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'insight' ),
		'popular_items'              => __( 'Popular Types', 'insight' ),
		'search_items'               => __( 'Search Types', 'insight' ),
		'not_found'                  => __( 'Not Found', 'insight' ),
		'no_terms'                   => __( 'No Types', 'insight' ),
		'items_list'                 => __( 'Types list', 'insight' ),
		'items_list_navigation'      => __( 'Types list navigation', 'insight' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'          	 => true,
	);
	register_taxonomy( 'resources_type', array( 'news', 'post' ), $args );

}
add_action( 'init', 'custom_taxonomy_type', 0 );
