<?php
/*
Plugin Name: DAC - Pillars
Description: <strong>Pillars</strong> Functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );


// Register Custom Taxonomy
function custom_taxonomy_pillars() {

	$labels = array(
		'name'                       => _x( 'Pillars', 'Taxonomy General Name', 'adi' ),
		'singular_name'              => _x( 'Pillar', 'Taxonomy Singular Name', 'adi' ),
		'menu_name'                  => __( 'Pillars', 'adi' ),
		'all_items'                  => __( 'All Pillars', 'adi' ),
		'parent_item'                => __( 'Parent Pillar', 'adi' ),
		'parent_item_colon'          => __( 'Parent Pillar:', 'adi' ),
		'new_item_name'              => __( 'New Pillar Name', 'adi' ),
		'add_new_item'               => __( 'Add New Pillar', 'adi' ),
		'edit_item'                  => __( 'Edit Pillar', 'adi' ),
		'update_item'                => __( 'Update Pillar', 'adi' ),
		'view_item'                  => __( 'View Pillar', 'adi' ),
		'separate_items_with_commas' => __( 'Separate Pillar with commas', 'adi' ),
		'add_or_remove_items'        => __( 'Add or remove Pillar', 'adi' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'adi' ),
		'popular_items'              => __( 'Popular Pillars', 'adi' ),
		'search_items'               => __( 'Search Pillars', 'adi' ),
		'not_found'                  => __( 'Not Found', 'adi' ),
		'no_terms'                   => __( 'No Pillars', 'adi' ),
		'items_list'                 => __( 'Pillars list', 'adi' ),
		'items_list_navigation'      => __( 'Pillars list navigation', 'adi' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'          	 => true,
	);
	register_taxonomy( 'pillar', array( 'post', 'news' ), $args );

}
add_action( 'init', 'custom_taxonomy_pillars', 0 );