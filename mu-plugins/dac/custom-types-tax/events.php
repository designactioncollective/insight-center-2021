<?php
/*
Plugin Name: DAC - Events CPT
Description: <strong>Events</strong> Functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// Register Custom Post Type
function custom_post_type_events() {

	$labels = array(
		'name'                  => _x( 'Events', 'Post Type General Name', 'adi' ),
		'singular_name'         => _x( 'Events', 'Post Type Singular Name', 'adi' ),
		'menu_name'             => __( 'Events', 'adi' ),
		'name_admin_bar'        => __( 'Events', 'adi' ),
		'archives'              => __( 'Events Archives', 'adi' ),
		'attributes'            => __( 'Events Attributes', 'adi' ),
		'parent_item_colon'     => __( 'Parent Events:', 'adi' ),
		'all_items'             => __( 'All Events', 'adi' ),
		'add_new_item'          => __( 'Add New Campaign or Program', 'adi' ),
		'add_new'               => __( 'Add New Events', 'adi' ),
		'new_item'              => __( 'New Events', 'adi' ),
		'edit_item'             => __( 'Edit Events', 'adi' ),
		'update_item'           => __( 'Update Events', 'adi' ),
		'view_item'             => __( 'View Events', 'adi' ),
		'view_items'            => __( 'View Events', 'adi' ),
		'search_items'          => __( 'Search Events', 'adi' ),
		'not_found'             => __( 'Not found', 'adi' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'adi' ),
		'featured_image'        => __( 'Featured Image', 'adi' ),
		'set_featured_image'    => __( 'Set featured image', 'adi' ),
		'remove_featured_image' => __( 'Remove featured image', 'adi' ),
		'use_featured_image'    => __( 'Use as featured image', 'adi' ),
		'insert_into_item'      => __( 'Insert into Events', 'adi' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'adi' ),
		'items_list'            => __( 'Events list', 'adi' ),
		'items_list_navigation' => __( 'Events list navigation', 'adi' ),
		'filter_items_list'     => __( 'Filter Events list', 'adi' ),
	);
	$args = array(
		'label'                 => __( 'Events', 'adi' ),
		'description'           => __( 'Post Type Description', 'insightcenter' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', 'excerpt' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'events', $args );

}
add_action( 'init', 'custom_post_type_events', 0 );
