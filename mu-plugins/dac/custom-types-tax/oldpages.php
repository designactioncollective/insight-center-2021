<?php
/*
Plugin Name: DAC - Old Pages
Description: <strong>Old Pages</strong> Functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// Register Custom Post Type
function custom_post_type_oldpages() {

	$labels = array(
		'name'                  => _x( 'Old Pages', 'Post Type General Name', 'todec' ),
		'singular_name'         => _x( 'Old Page', 'Post Type Singular Name', 'todec' ),
		'menu_name'             => __( 'Old Pages', 'todec' ),
		'name_admin_bar'        => __( 'Old Pages', 'todec' ),
		'archives'              => __( 'Old Pages Archives', 'todec' ),
		'attributes'            => __( 'Old Pages Attributes', 'todec' ),
		'parent_item_colon'     => __( 'Parent Old Pages:', 'todec' ),
		'all_items'             => __( 'All Old Pages', 'todec' ),
		'add_new_item'          => __( 'Add New Campaign or Program', 'todec' ),
		'add_new'               => __( 'Add New Old Pages', 'todec' ),
		'new_item'              => __( 'New Old Pages', 'todec' ),
		'edit_item'             => __( 'Edit Old Pages', 'todec' ),
		'update_item'           => __( 'Update Old Pages', 'todec' ),
		'view_item'             => __( 'View Old Pages', 'todec' ),
		'view_items'            => __( 'View Old Pages', 'todec' ),
		'search_items'          => __( 'Search Old Pages', 'todec' ),
		'not_found'             => __( 'Not found', 'todec' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'todec' ),
		'featured_image'        => __( 'Featured Image', 'todec' ),
		'set_featured_image'    => __( 'Set featured image', 'todec' ),
		'remove_featured_image' => __( 'Remove featured image', 'todec' ),
		'use_featured_image'    => __( 'Use as featured image', 'todec' ),
		'insert_into_item'      => __( 'Insert into Old Pages', 'todec' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'todec' ),
		'items_list'            => __( 'Old Pages list', 'todec' ),
		'items_list_navigation' => __( 'Old Pages list navigation', 'todec' ),
		'filter_items_list'     => __( 'Filter Old Pages list', 'todec' ),
	);
	$args = array(
		'label'                 => __( 'Old Pages', 'todec' ),
		'description'           => __( 'Post Type Description', 'todec' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', 'excerpt' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'old-pages', $args );

}
add_action( 'init', 'custom_post_type_oldpages', 0 );
