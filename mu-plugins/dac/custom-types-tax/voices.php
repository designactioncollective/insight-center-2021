<?php
/*
Plugin Name: DAC - Voices CPT
Description: <strong>Voices</strong> Functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// Register Custom Post Type
function custom_post_type_voices() {

	$labels = array(
		'name'                  => _x( 'Voices', 'Post Type General Name', 'insightcenter' ),
		'singular_name'         => _x( 'Voice', 'Post Type Singular Name', 'insightcenter' ),
		'menu_name'             => __( 'Voices', 'insightcenter' ),
		'name_admin_bar'        => __( 'Voices', 'insightcenter' ),
		'archives'              => __( 'Voices Archives', 'insightcenter' ),
		'attributes'            => __( 'Voices Attributes', 'insightcenter' ),
		'parent_item_colon'     => __( 'Parent Voices:', 'insightcenter' ),
		'all_items'             => __( 'All Voices', 'insightcenter' ),
		'add_new_item'          => __( 'Add New Voice', 'insightcenter' ),
		'add_new'               => __( 'Add New Voice', 'insightcenter' ),
		'new_item'              => __( 'New Voice', 'insightcenter' ),
		'edit_item'             => __( 'Edit Voice', 'insightcenter' ),
		'update_item'           => __( 'Update Voice', 'insightcenter' ),
		'view_item'             => __( 'View Voices', 'insightcenter' ),
		'view_items'            => __( 'View Voices', 'insightcenter' ),
		'search_items'          => __( 'Search Voices', 'insightcenter' ),
		'not_found'             => __( 'Not found', 'insightcenter' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'insightcenter' ),
		'featured_image'        => __( 'Featured Image', 'insightcenter' ),
		'set_featured_image'    => __( 'Set featured image', 'insightcenter' ),
		'remove_featured_image' => __( 'Remove featured image', 'insightcenter' ),
		'use_featured_image'    => __( 'Use as featured image', 'insightcenter' ),
		'insert_into_item'      => __( 'Insert into Voices', 'insightcenter' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'insightcenter' ),
		'items_list'            => __( 'Voices list', 'insightcenter' ),
		'items_list_navigation' => __( 'Voices list navigation', 'insightcenter' ),
		'filter_items_list'     => __( 'Filter Voices list', 'insightcenter' ),
	);
	$args = array(
		'label'                 => __( 'Voices', 'insightcenter' ),
		'description'           => __( 'Post Type Description', 'insightcenter' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', 'excerpt' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'voices', $args );

}
add_action( 'init', 'custom_post_type_voices', 0 );
